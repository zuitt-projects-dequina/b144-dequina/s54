let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(item) {
    collection[collection.length] = item;
    return collection;
}

// Dequeue the first element
function dequeue() {
    // collection.splice(0,1);
    // return collection;
	let newCollection = []
	for (let i = 0; i < collection.length; i++) {
		if(i != 0) {
			newCollection[i-1] = collection[i];
		}
	}
	collection = newCollection;
	return collection
}


// Get first Element
function front() {
    return collection[0];
}


// Getting the queue size
function size() {
    return collection.length;
}


// Checking if queue is empty
function isEmpty() {
    if (collection.lenth === 0) {
        return true;
    }
    else {
        return false;
    }
}


module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
    
};